1. Clone the IPython repo from http://bitbucket.org/mathharbor/skyfall-notebook
2. Navigate to the ipython directory and run pip install -e ".[notebook]"
3. Next, install skyfall's dependencies by navigating to the skyfall directory and running pip install -r requirements.txt